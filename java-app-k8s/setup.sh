#!/bin/sh
#
# setup.sh - CI/CD bootstrapping script
#
# See function 'check_env_vars()' for the required environment variables.
#
##

set -e

# Settings

AwsCliVersion="< 2.0"

KubectlVersion="$( wget -q -O - https://storage.googleapis.com/kubernetes-release/release/stable.txt )"
KubectlPath="/usr/local/bin/kubectl"

# Functions

check_env_vars()
{
    local var

    echo "Checking required environment variables..."

    for var in \
        K8S_DEPLOY_USER \
        K8S_CLUSTER_CA \
        K8S_CLUSTER_SERVER \
        K8S_CLIENT_CRT \
        K8S_CLIENT_KEY
    do
        if [ -z "$( eval "echo \$$var" )" ] ; then
            echo "Fatal: variable $var is not set." >&2
            exit 1
        fi
    done
}

install_awscli()
{
    apk add --no-cache \
        py3-pip

    pip3 install --timeout 5 --disable-pip-version-check \
        "awscli $AwsCliVersion"
}

install_kubectl()
{
    wget -q -O "$KubectlPath" "https://storage.googleapis.com/kubernetes-release/release/${KubectlVersion}/bin/linux/amd64/kubectl"
    chmod -v +x "$KubectlPath"

    mkdir -p ~/.kube

    (
        # Disable debug so the secrets are not sent to the console
        set +x

        cat <<EOF > ~/.kube/config
apiVersion: v1
kind: Config
preferences: {}

clusters:
- name: k8s-hmg
  cluster:
    certificate-authority-data: ${K8S_CLUSTER_CA}
    server: ${K8S_CLUSTER_SERVER}

contexts:
- name: k8s-default
  context:
    cluster: k8s-hmg
    user: deploy

current-context: k8s-default

users:
- name: deploy
  user:
    username: ${K8S_DEPLOY_USER}
    client-certificate-data: ${K8S_CLIENT_CRT}
    client-key-data: ${K8S_CLIENT_KEY}
EOF

        echo "kubectl config '~/.kube/config' has been created!"
    )
}

# Main

main()
{
    # Check
    check_env_vars
    set -x
    command -v docker

    # Install
    install_awscli
    install_kubectl

    # Info
    aws --version
    docker version
    kubectl version
}

main
